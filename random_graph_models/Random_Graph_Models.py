# -*- coding: utf-8 -*-
"""
@author: Saumitra Kulkarni
"""
import random
import numpy as np
from BFS import BFS

# A class to represent a graph generation object
class Graph_Generator:
    # Constructor
    def __init__(self):
        # Adjecency list
        self.adj_list = dict()
        
    def add_edge(self, u: int, v: int):
        """
        Adds target node to adjecency list of source node

        Parameters
        ----------
        u : int
            Source Node.
        v : int
            Target Node.

        """
        return self.adj_list[u].append(v)
    
    def remove_node(self, u: int):
        """
        Removes given node from adjecency list and list of nodes of the Graph

        Parameters
        ----------
        u : int
            Node from the Graph

        """
        del self.adj_list[u]
        self.nodes.remove(u)
        return None
   
    def Random_Graph(self, n: int, m: int, seed = None):
        """
        Returns a G(n,m) random graph

        Parameters
        ----------
        n : int
            Number of nodes.
        m : int
            Number of edges.
        seed : int, optional
            Random seed for replication. The default is None.

        """
        # Set random seed
        if seed == None:
            pass
        else:
            random.seed(int(seed))
        # Total number of edges in the network
        self.number_of_edges = int(m)
        # Total number of nodes in the network        
        self.number_of_nodes = int(n)
        # Nodes in the network
        self.nodes = list(range(self.number_of_nodes))
        # Source nodes
        for k in self.nodes:
            self.adj_list[k] = [] 
        # Counter for generated edges
        count = 0
        # Loop until generated edges are equal to number of edges 
        while count < int(m):
            # Randomly select first node from network
            random_node1 = random.choice(self.nodes)
            # Randomly select second node from network
            random_node2 = random.choice(self.nodes)
            # Generate an edge between those two nodes
            self.add_edge(random_node1, random_node2)
            self.add_edge(random_node2, random_node1)
            # Count it as a single edge
            count += 1
            
        return self
            
    def Erdos_Reyni_Graph(self, n: int, p: float, seed = None):
        """
        Returns a G(n,p) random graph, widely known as Erdos Renyi Graph

        Parameters
        ----------
        n : int
            Number of nodes.
        p : float
            Probability of an edge between two nodes.
        seed : int, optional
            Random seed for replication. The default is None.
            
        """
        # Set random seed
        if seed == None:
            pass
        else:
            random.seed(int(seed))
        # Total number of nodes in the network
        self.number_of_nodes = int(n)
        # Probability of edges between nodes
        self.probability_of_edge = p
        # Nodes in the network
        self.nodes = list(range(self.number_of_nodes))
        # Source nodes
        for k in self.nodes:
            self.adj_list[k] = [] 
        # Loop over all nodes in the network
        for i in range(self.number_of_nodes - 1):
            # Loop over all nodes in the network
            for j in range(i+1, self.number_of_nodes):
                # If node i is less than j
                if (i < j):
                    # Take random number R.
                    R = random.random()
                    # Check if R<P add the edge to the graph else ignore.
                    if (R < float(self.probability_of_edge)):
                        self.add_edge(i, j)
                        self.add_edge(j, i)
                        
        return self
    


def average_degree(G):
    """
    Returns the average degree of the given network

    Parameters
    ----------
    G : Graph
        Graph Generator.

    """
    return (2*G.number_of_edges)/G.number_of_nodes

def global_clustering_coeff(G):
    """
    Returns the global clustering coefficient of the given network
    
    Parameters
    ----------
    G : Graph
        Graph Generator.

    """
    # Local Clustering Coefficient
    lcc = dict()
    # Loop over all nodes in the network
    for node in G.nodes:
        # Neighbours of node
        neighbours = G.adj_list[node]
        # Total number of neighbours of node
        n_neighbors = len(neighbours)
        # Total number of links in network
        n_links = 0
        # If total number of neighbours are greater than 1
        if n_neighbors > 1:
            # Loop over all nodes in the network
            for node1 in neighbours:
                # Loop over all nodes in the network
                for node2 in neighbours:
                    # If node1 is a neighbour of node2
                    if node1 in G.adj_list[node2]:
                        # Count it as one link between those nodes
                        n_links += 1
            # Divide whole count by 2
            n_links /= 2
            # Assign local clustering coefficient to its resp. node
            lcc[node] = n_links/(0.5*n_neighbors*(n_neighbors-1))
        # If total number of neighbours are not greater than 1
        else:
            # Assign local clustering coefficient as 0 to its resp. node
            lcc[node] = 0
            
    return np.mean(list(lcc.values()))

def fractional_size_of_GC(average_degree: float, itr = 50, S_init = 0.01):
    """
    Returns the fractional size of giant component in Erdős–Rényi graph model 
    for a given value of average degree.
    
    Parameters
    ----------
    average_degree : float
        Average degree of the given network.
    itr : int, optional
        Number of iteration. The default is 50.
        Choose as per required degree of convergence.
    S_init : float, optional
        Initial value of fractional size. The default is 0.01.

    """
    # Initial guess of fractional size
    S = S_init
    # Loop for number of iterations 
    for i in range(itr):
        # Plug-in initial guess in expression and assign that to variable S again
        # This gives a better approximation at each iteration
        S = 1 - np.exp( - average_degree * S )
        
    return S

def connected_components(G, sizes = False):
    """
    Return the list of lists of nodes from each connected component and
    sizes of each connected component in the network

    Parameters
    ----------
    G : Graph
        Graph Generator. 
    sizes : bool, optional
        Set as True if size of connected components are also to be returned. 
        The default is False.
        
    """
    # Dictionary for visited nodes
    visited = dict.fromkeys(list(G.adj_list.keys()), False)
    # List of all the connected components
    data = list()
    # Loop over all the nodes in the network
    for i in sorted(list(G.adj_list.keys())):
        if visited[i] == False:
            level, bfs_traversal = BFS(G, i)
        # Append path traversed by BFS to connected components
        data.append(sorted(bfs_traversal))
    # Find unique connected components with 2 or more nodes
    connected_components = [list(x) for x in set(tuple(x) for x in data)]
    # Calculate the sizes of connected components
    size_of_connected_components = [len(x) for x in connected_components]
    
    if sizes == True:
        return connected_components, size_of_connected_components
    elif sizes == False:
        return connected_components
    # return connected_components

def average_size_of_small_component(sc_nodes, G, itr: int):
    """
    Returns average size of small component to which a randomly chosen node in 
    Erdős–Rényi random graph belongs.

    Parameters
    ----------
    sc_nodes : array_like, 1-D array
        Array_like data structure of nodes from the small components.
    G : Graph
        Graph Generator.
    itr : int, optional
        Number of random nodes to choose from the small components.
        Choose as per requirment.

    """
    # Randomly choose nodes from small components with replacement
    random_nodes = np.random.choice(sc_nodes, size=itr, replace=True)
    # Dictionary for visited nodes
    visited = dict.fromkeys(random_nodes, False)
    # List of all the connected components
    size_of_small_cc = list()
    # Loop over all the nodes in the network
    for i in sorted(random_nodes):
        if visited[i] == False:
            level, bfs_traversal = BFS(G, i)
        # Append path traversed by BFS to connected components
        size_of_small_cc.append(len(bfs_traversal))

    return np.mean(size_of_small_cc)
    
# Driver code
if __name__ == '__main__':
    # Random Graph G(n,m)
    print("\nRANDOM GRAPH MODEL - G(n,m)")
    n = str(input ("Enter number of nodes (n): "))
    m = str(input ("Enter number of edges (m): "))
    G = Graph_Generator().Random_Graph(n, m)
    print("\nGlobal Clustering Coefficient: ", global_clustering_coeff(G))
    
    # Erdos Reyni Graph G(n,p)
    print("\nERDOS REYNI GRAPH MODEL - G(n,p)")
    n = str(input ("Enter number of nodes (n): "))
    p = str(input ("Enter probability of edges between nodes (p): "))
    G = Graph_Generator().Erdos_Reyni_Graph(n, p)
    print("\nGlobal Clustering Coefficient: ", global_clustering_coeff(G))
    
