# -*- coding: utf-8 -*-
"""
@author: Saumitra Kulkarni
"""
from queue import Queue

def BFS(G, source):
    """
    Return the dictionary with nodes in the network as keys and respective 
    distances from source node as values
    """
    # Dictionary for visited nodes
    visited = dict.fromkeys(list(G.adj_list.keys()), False)
    # Dictionary for level (distance from source node) in network
    level = dict.fromkeys(list(G.adj_list.keys()), -1)
    # List for path of node traverse
    bfs_traversal = list()
    # Initialize a queue
    queue = Queue()
    # Intial values for source node
    visited[source] = True
    level[source] = 0
    # Add source node to the queue
    queue.put(source)
    # Run until queue becomes empty
    while not queue.empty(): 
        # Remove and return an item from queue
        u = queue.get()
        # Add current visited node to traversal path
        bfs_traversal.append(u)
        # Loop over neighbours of current visited node
        for v in G.adj_list[u]: 
            if not visited[v]:
                visited[v] = True
                level[v] = level[u] + 1
                queue.put(v)
    
    return level, bfs_traversal