# -*- coding: utf-8 -*-
"""
@author: Saumitra Kulkarni
"""
import numpy as np
import pandas as pd
from queue import Queue
import time

# A class to represent a graph object
class Graph:
    # Constructor
    def __init__(self, fname):
        
        # Network data read in edge list data structure
        try:
            self.edge_list = np.loadtxt(fname, dtype=int, delimiter=',')
        except:
            self.edge_list = pd.read_csv(fname, usecols=[0,1]).to_numpy(na_value=0)
        # Total number of edges and nodes in the network
        self.number_of_edges = len(self.edge_list)
        self.number_of_nodes = len(np.unique(self.edge_list))
        # Adjecency list
        self.adj_list = dict()
        
        # Source nodes
        for j in range(self.number_of_edges):
            if self.edge_list[j][0] in self.adj_list:
                self.adj_list[self.edge_list[j][0]].append(self.edge_list[j][1])
            else:   
                self.adj_list[self.edge_list[j][0]] = [self.edge_list[j][1]]
        # Target nodes
        for j in range(self.number_of_edges):
            if self.edge_list[j][1] in self.adj_list:
                self.adj_list[self.edge_list[j][1]].append(self.edge_list[j][0])
            else:   
                self.adj_list[self.edge_list[j][1]] = [self.edge_list[j][0]]
        # Isolated nodes       
        for k in np.unique(self.edge_list):
            if k not in self.adj_list:
                self.adj_list[k] = []

    def naive_BFS(self, source):
        """
        Return the dictionary with nodes in the network as keys and respective 
        distances from source node as values
        """
        # Dictionary to store the distances with value -1
        level = dict.fromkeys(list(self.adj_list.keys()), -1)
        # Distance variable to keep track of maximum distance known so far
        d = 0
        # Set distance from source to itself equal to 0
        level[source] = 0
        # Run until value of d is exhausted
        while d in level.values():
            # Nodes at distance d from the source
            nodes_at_d = [i for i,n in level.items() if n == d]
            # Loop over nodes at distance d from the source
            for n in nodes_at_d:
                # Neighbours of nodes at distance d from the source
                neighbours = self.adj_list[n]
                # Loop over those neighbours
                for nodes in neighbours:
                    if level[nodes] == -1:
                        level[nodes] = d+1    
            # Increase the value of distance by 1
            d += 1
        
        return level
        
    def efficient_BFS(self, source):
        """
        Return the dictionary with nodes in the network as keys and respective 
        distances from source node as values
        """
        # Dictionary for visited nodes
        visited = dict.fromkeys(list(self.adj_list.keys()), False)
        # Dictionary for level (distance from source node) in network
        level = dict.fromkeys(list(self.adj_list.keys()), -1)
        # List for path of node traverse
        bfs_traversal = list()
        # Initialize a queue
        queue = Queue()
        # Intial values for source node
        visited[source] = True
        level[source] = 0
        # Add source node to the queue
        queue.put(source)
        # Run until queue becomes empty
        while not queue.empty(): 
            # Remove and return an item from queue
            u = queue.get()
            # Add current visited node to traversal path
            bfs_traversal.append(u)
            # Loop over neighbours of current visited node
            for v in self.adj_list[u]: 
                if not visited[v]:
                    visited[v] = True
                    level[v] = level[u] + 1
                    queue.put(v)
        
        return level, bfs_traversal

    def average_path_length(self, BFS_version):
        """
        Return the average path length in the network
        """
        # Time at the start of program execution
        start_time = float(time.time())
        # Initialize value of average path length
        avg_path_length = 0
        # Total number of nodes
        n = self.number_of_nodes
        # Loop over all the nodes in the network
        for i in sorted(list(self.adj_list.keys())):
            if BFS_version == 'naive':
                level = self.naive_BFS(i)
            elif BFS_version == 'efficient':
                level, bfs_traversal = self.efficient_BFS(i)
            # Calculate average path length and add it to total value
            avg_path_length += sum(list(level.values())) / (n*(n-1))
        # Print the total time required for program execution
        print(f"\nComputer time required: {float(time.time() - start_time)} seconds")
        # Print the total number of connected components
        return print('The average path length of network is', avg_path_length)

    def number_of_connected_components(self):
        """
        Return the total number of connected components in the network
        """
        # Time at the start of program execution
        start_time = float(time.time())
        # Dictionary for visited nodes
        visited = dict.fromkeys(list(self.adj_list.keys()), False)
        # List of all the connected components
        connected_components = list()
        # Loop over all the nodes in the network
        for i in sorted(list(self.adj_list.keys())):
            if visited[i] == False:
                level, bfs_traversal = self.efficient_BFS(i)
            # Append path traversed by BFS to connected components
            connected_components.append(sorted(bfs_traversal))
        # Count isolated nodes as components 
        count = 0
        for k in self.adj_list.values():
            if k == [0]:
                count += 1
        # Set axis for finding unique connected components
        ax = 0 if count == 0 else None
        # Find unique connected components with 2 or more nodes
        unique, unique_indices = np.unique(connected_components, axis=ax, return_index=True)
        # Calculate the total number of connected components
        total = (count) + len(unique_indices)
        # Print the total time required for program execution
        print(f"\nComputer time required: {float(time.time() - start_time)} seconds")
        print('Size of the components of network is', np.size(unique))
        # Print the total number of connected components
        print('The number of connected components of network are', total)
        return None
    
# Driver code
if __name__ == '__main__':
    
    fname = str(input ("Enter path of the network edgelist: "))
    g = Graph(fname)
    
    print('\nNaive Implementation of BFS')
    g.average_path_length(BFS_version = 'naive')
    print('\nEfficient Implementation of BFS')
    g.average_path_length(BFS_version = 'efficient')
    print('\nTotal Number of Connected Components')
    g.number_of_connected_components()
