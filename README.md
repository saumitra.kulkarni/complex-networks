# Network Algorithms
Python implementations of some of the network algorithms from scratch.

This repository contains following codes:

`breadth_first_search`: Algorithms to find the shortest network distance from a given source node $s$ to every other node in the same component as $s$ in a given network. This repository contains both naive and efficient implementations of the Breadth First Search (BSF) algorithm. It also has functions which apply the BFS algorithm to find average path length and total number of connected components in a given network.

`random_graph_models`: Algorithms to draw a random graph from both $G(n,m)$ and $G(n, p)$ ensembles. This repository also contains the functions to compute average degree, global clustering coefficient, fractional size of the giant component, connected components, average size of the small components of a given random graph. Some of these functions additionally use efficient implementation of BFS algorithm.

All these codes require network edgelist as input with following attributes:

- Each line in the data file should have indices of nodes at the two ends of the edge separated by comma. 
- Isolated nodes should be written only as single entry on a separate line.
- Each edge in the network appears exactly once in each dataset:
    - For undirected networks, the order of the node indices doesn’t matter, and so the edge $(i,j)$ is saved such that $i$ < $j$. 
    - For directed networks, the edge $(i,j)$ means that the edge points from $j$ to $i$ i.e. from the second entry to the first entry on each line.
